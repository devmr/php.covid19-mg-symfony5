<?php

namespace App\Repository;

use App\Entity\DataDistrict;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataDistrict|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataDistrict|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataDistrict[]    findAll()
 * @method DataDistrict[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataDistrictRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataDistrict::class);
    }

    // /**
    //  * @return DataDistrict[] Returns an array of DataDistrict objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataDistrict
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', explode(',',$ids))
            ->getQuery()
            ->getResult()
        ;

        return $qb;
    }

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\DataDistrict a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }
}
