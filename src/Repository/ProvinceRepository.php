<?php

namespace App\Repository;

use App\Entity\Province;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Province|null find($id, $lockMode = null, $lockVersion = null)
 * @method Province|null findOneBy(array $criteria, array $orderBy = null)
 * @method Province[]    findAll()
 * @method Province[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProvinceRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Province::class);
    }

    // /**
    //  * @return Province[] Returns an array of Province objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Province
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', explode(',',$ids))
            ->getQuery()
            ->getResult()
        ;

        return $qb;
    }

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\Province a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
