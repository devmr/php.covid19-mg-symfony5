<?php

namespace App\Repository;

use App\Entity\DataSex;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataSex|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataSex|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataSex[]    findAll()
 * @method DataSex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataSexRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataSex::class);
    }

    // /**
    //  * @return DataSex[] Returns an array of DataSex objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataSex
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\DataSex a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
