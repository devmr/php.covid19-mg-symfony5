<?php

namespace App\Repository;

use App\Entity\DataDateType;
use App\Entity\TypeCase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataDateType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataDateType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataDateType[]    findAll()
 * @method DataDateType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataDateTypeRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataDateType::class);
    }

    // /**
    //  * @return DataAll[] Returns an array of DataAll objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataAll
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllOrderByDate($sort)
    {

        if (!in_array($sort, ['ASC','DESC'])) {
            return false;
        }

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT b, a FROM App\Entity\DataDateType a INNER JOIN a.date b INNER JOIN a.typeCase c  ORDER BY c.position ASC, b.date $sort");

        return $query->getResult();
    }

    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', explode(',',$ids))
            ->getQuery()
            ->getResult()
        ;

        return $qb;
    }

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\DataDateType a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
