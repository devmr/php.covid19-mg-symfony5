<?php

namespace App\Repository;

use App\Entity\Commune;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Commune|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commune|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commune[]    findAll()
 * @method Commune[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommuneRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commune::class);
    }

    // /**
    //  * @return Commune[] Returns an array of Commune objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commune
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\Commune a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
