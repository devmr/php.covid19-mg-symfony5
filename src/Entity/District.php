<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\ApiPlatform\DuplicateDistrict;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/districts/{id}/duplicate",
 *              "controller"=DuplicateDistrict::class,
 *          }
 *     },
 *     collectionOperations=
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *      }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "name"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "name": "ipartial",
 *          "province": "exact",
 *          "region": "exact"
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\DistrictRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class District
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $disabledAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="districts")
     * @Groups({"group:read"})
     */
    private $province;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="districts")
     * @Groups({"group:read"})
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commune", mappedBy="district")
     */
    private $communes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quartier", mappedBy="district")
     */
    private $quartiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDistrict", mappedBy="district")
     */
    private $dataDistricts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateDistrict", mappedBy="district")
     */
    private $dataDateDistricts;

    public function __construct()
    {
        $this->communes = new ArrayCollection();
        $this->quartiers = new ArrayCollection();
        $this->dataDistricts = new ArrayCollection();
        $this->dataDateDistricts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): self
    {
        $this->province = $province;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection|Commune[]
     */
    public function getCommunes(): Collection
    {
        return $this->communes;
    }

    public function addCommune(Commune $commune): self
    {
        if (!$this->communes->contains($commune)) {
            $this->communes[] = $commune;
            $commune->setDistrict($this);
        }

        return $this;
    }

    public function removeCommune(Commune $commune): self
    {
        if ($this->communes->contains($commune)) {
            $this->communes->removeElement($commune);
            // set the owning side to null (unless already changed)
            if ($commune->getDistrict() === $this) {
                $commune->setDistrict(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quartier[]
     */
    public function getQuartiers(): Collection
    {
        return $this->quartiers;
    }

    public function addQuartier(Quartier $quartier): self
    {
        if (!$this->quartiers->contains($quartier)) {
            $this->quartiers[] = $quartier;
            $quartier->setDistrict($this);
        }

        return $this;
    }

    public function removeQuartier(Quartier $quartier): self
    {
        if ($this->quartiers->contains($quartier)) {
            $this->quartiers->removeElement($quartier);
            // set the owning side to null (unless already changed)
            if ($quartier->getDistrict() === $this) {
                $quartier->setDistrict(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDistrict[]
     */
    public function getDataDistricts(): Collection
    {
        return $this->dataDistricts;
    }

    public function addDataDistrict(DataDistrict $dataDistrict): self
    {
        if (!$this->dataDistricts->contains($dataDistrict)) {
            $this->dataDistricts[] = $dataDistrict;
            $dataDistrict->setDistrict($this);
        }

        return $this;
    }

    public function removeDataDistrict(DataDistrict $dataDistrict): self
    {
        if ($this->dataDistricts->contains($dataDistrict)) {
            $this->dataDistricts->removeElement($dataDistrict);
            // set the owning side to null (unless already changed)
            if ($dataDistrict->getDistrict() === $this) {
                $dataDistrict->setDistrict(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateDistrict[]
     */
    public function getDataDateDistricts(): Collection
    {
        return $this->dataDateDistricts;
    }

    public function addDataDateDistrict(DataDateDistrict $dataDateDistrict): self
    {
        if (!$this->dataDateDistricts->contains($dataDateDistrict)) {
            $this->dataDateDistricts[] = $dataDateDistrict;
            $dataDateDistrict->setDistrict($this);
        }

        return $this;
    }

    public function removeDataDateDistrict(DataDateDistrict $dataDateDistrict): self
    {
        if ($this->dataDateDistricts->contains($dataDateDistrict)) {
            $this->dataDateDistricts->removeElement($dataDateDistrict);
            // set the owning side to null (unless already changed)
            if ($dataDateDistrict->getDistrict() === $this) {
                $dataDateDistrict->setDistrict(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setProvinceValue()
    {
        $this->province = $this->getRegion()->getProvince();
    }
}
