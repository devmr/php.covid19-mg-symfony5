<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\ApiPlatform\DuplicateProvince;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/provinces/{id}/duplicate",
 *              "controller"=DuplicateProvince::class,
 *          }
 *     },
 *     collectionOperations =
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           }
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "name"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "name": "ipartial",
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProvinceRepository")
 */
class Province
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"province_list"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Region", mappedBy="province")
     *
     */
    private $regions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\District", mappedBy="province")
     */
    private $districts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commune", mappedBy="province")
     */
    private $communes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quartier", mappedBy="province")
     */
    private $quartiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataProvince", mappedBy="province")
     */
    private $dataProvinces;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateProvince", mappedBy="province")
     */
    private $dataDateProvinces;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
        $this->districts = new ArrayCollection();
        $this->communes = new ArrayCollection();
        $this->quartiers = new ArrayCollection();
        $this->dataProvinces = new ArrayCollection();
        $this->dataDateProvinces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * @return Collection|Region[]
     */
    public function getRegions(): Collection
    {
        return $this->regions;
    }

    public function addRegion(Region $region): self
    {
        if (!$this->regions->contains($region)) {
            $this->regions[] = $region;
            $region->setProvince($this);
        }

        return $this;
    }

    public function removeRegion(Region $region): self
    {
        if ($this->regions->contains($region)) {
            $this->regions->removeElement($region);
            // set the owning side to null (unless already changed)
            if ($region->getProvince() === $this) {
                $region->setProvince(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|District[]
     */
    public function getDistricts(): Collection
    {
        return $this->districts;
    }

    public function addDistrict(District $district): self
    {
        if (!$this->districts->contains($district)) {
            $this->districts[] = $district;
            $district->setProvince($this);
        }

        return $this;
    }

    public function removeDistrict(District $district): self
    {
        if ($this->districts->contains($district)) {
            $this->districts->removeElement($district);
            // set the owning side to null (unless already changed)
            if ($district->getProvince() === $this) {
                $district->setProvince(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commune[]
     */
    public function getCommunes(): Collection
    {
        return $this->communes;
    }

    public function addCommune(Commune $commune): self
    {
        if (!$this->communes->contains($commune)) {
            $this->communes[] = $commune;
            $commune->setProvince($this);
        }

        return $this;
    }

    public function removeCommune(Commune $commune): self
    {
        if ($this->communes->contains($commune)) {
            $this->communes->removeElement($commune);
            // set the owning side to null (unless already changed)
            if ($commune->getProvince() === $this) {
                $commune->setProvince(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quartier[]
     */
    public function getQuartiers(): Collection
    {
        return $this->quartiers;
    }

    public function addQuartier(Quartier $quartier): self
    {
        if (!$this->quartiers->contains($quartier)) {
            $this->quartiers[] = $quartier;
            $quartier->setProvince($this);
        }

        return $this;
    }

    public function removeQuartier(Quartier $quartier): self
    {
        if ($this->quartiers->contains($quartier)) {
            $this->quartiers->removeElement($quartier);
            // set the owning side to null (unless already changed)
            if ($quartier->getProvince() === $this) {
                $quartier->setProvince(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataProvince[]
     */
    public function getDataProvinces(): Collection
    {
        return $this->dataProvinces;
    }

    public function addDataProvince(DataProvince $dataProvince): self
    {
        if (!$this->dataProvinces->contains($dataProvince)) {
            $this->dataProvinces[] = $dataProvince;
            $dataProvince->setProvince($this);
        }

        return $this;
    }

    public function removeDataProvince(DataProvince $dataProvince): self
    {
        if ($this->dataProvinces->contains($dataProvince)) {
            $this->dataProvinces->removeElement($dataProvince);
            // set the owning side to null (unless already changed)
            if ($dataProvince->getProvince() === $this) {
                $dataProvince->setProvince(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateProvince[]
     */
    public function getDataDateProvinces(): Collection
    {
        return $this->dataDateProvinces;
    }

    public function addDataDateProvince(DataDateProvince $dataDateProvince): self
    {
        if (!$this->dataDateProvinces->contains($dataDateProvince)) {
            $this->dataDateProvinces[] = $dataDateProvince;
            $dataDateProvince->setProvince($this);
        }

        return $this;
    }

    public function removeDataDateProvince(DataDateProvince $dataDateProvince): self
    {
        if ($this->dataDateProvinces->contains($dataDateProvince)) {
            $this->dataDateProvinces->removeElement($dataDateProvince);
            // set the owning side to null (unless already changed)
            if ($dataDateProvince->getProvince() === $this) {
                $dataDateProvince->setProvince(null);
            }
        }

        return $this;
    }
}
