<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *     },
 *     collectionOperations =
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\SexRepository")
 */
class Sex implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"group:read"})
     */
    private $settings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataSex", mappedBy="Sex")
     */
    private $dataSexes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateSex", mappedBy="Sex")
     */
    private $dataDateSexes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Translation", inversedBy="sexes")
     * @JoinTable(name="sex_translation_db")
     * @Groups({"group:read"})
     */
    private $TranslationDb;

    public function __construct()
    {
        $this->dataSexes = new ArrayCollection();
        $this->dataDateSexes = new ArrayCollection();
        $this->TranslationDb = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTranslatedName($locale)
    {
        return $this->translate($locale)->getName();
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getSettings(): ?string
    {
        return $this->settings;
    }

    public function setSettings(?string $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @return Collection|DataSex[]
     */
    public function getDataSexes(): Collection
    {
        return $this->dataSexes;
    }

    public function addDataSex(DataSex $dataSex): self
    {
        if (!$this->dataSexes->contains($dataSex)) {
            $this->dataSexes[] = $dataSex;
            $dataSex->setSex($this);
        }

        return $this;
    }

    public function removeDataSex(DataSex $dataSex): self
    {
        if ($this->dataSexes->contains($dataSex)) {
            $this->dataSexes->removeElement($dataSex);
            // set the owning side to null (unless already changed)
            if ($dataSex->getSex() === $this) {
                $dataSex->setSex(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateSex[]
     */
    public function getDataDateSexes(): Collection
    {
        return $this->dataDateSexes;
    }

    public function addDataDateSex(DataDateSex $dataDateSex): self
    {
        if (!$this->dataDateSexes->contains($dataDateSex)) {
            $this->dataDateSexes[] = $dataDateSex;
            $dataDateSex->setSex($this);
        }

        return $this;
    }

    public function removeDataDateSex(DataDateSex $dataDateSex): self
    {
        if ($this->dataDateSexes->contains($dataDateSex)) {
            $this->dataDateSexes->removeElement($dataDateSex);
            // set the owning side to null (unless already changed)
            if ($dataDateSex->getSex() === $this) {
                $dataDateSex->setSex(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Translation[]
     */
    public function getTranslationDb(): Collection
    {
        return $this->TranslationDb;
    }

    public function addTranslationDb(Translation $translationDb): self
    {
        if (!$this->TranslationDb->contains($translationDb)) {
            $this->TranslationDb[] = $translationDb;
        }

        return $this;
    }

    public function removeTranslationDb(Translation $translationDb): self
    {
        if ($this->TranslationDb->contains($translationDb)) {
            $this->TranslationDb->removeElement($translationDb);
        }

        return $this;
    }

}
