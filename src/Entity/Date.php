<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
/**
 * @ApiResource()
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "date": "exact",
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\DateRepository")
 * @Table(name="date",uniqueConstraints={@UniqueConstraint(name="date_unique", columns={"date"})})
 */
class Date
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"group:read"})
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateType", mappedBy="date")
     */
    private $dataDateTypes;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateProvince", mappedBy="date")
     */
    private $dataDateProvinces;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateRegion", mappedBy="date")
     */
    private $dataDateRegions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateDistrict", mappedBy="date")
     */
    private $dataDateDistricts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateSex", mappedBy="date")
     */
    private $dataDateSexes;

    public function __construct()
    {
        $this->dataDateTypes = new ArrayCollection();
        $this->dataDateProvinces = new ArrayCollection();
        $this->dataDateRegions = new ArrayCollection();
        $this->dataDateDistricts = new ArrayCollection();
        $this->dataDateSexes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|DataDateType[]
     */
    public function getDataDateType(): Collection
    {
        return $this->dataDateTypes;
    }

    public function addDataDateType(DataDateType $dataDateType): self
    {
        if (!$this->dataDateTypes->contains($dataDateType)) {
            $this->dataDateTypes[] = $dataDateType;
            $dataDateType->setDate($this);
        }

        return $this;
    }

    public function removeDataDateType(DataDateType $dataDateType): self
    {
        if ($this->dataDateTypes->contains($dataDateType)) {
            $this->dataDateTypes->removeElement($dataDateType);
            // set the owning side to null (unless already changed)
            if ($dataAll->getDate() === $this) {
                $dataAll->setDate(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|DataDateProvince[]
     */
    public function getDataDateProvinces(): Collection
    {
        return $this->dataDateProvinces;
    }

    public function addDataDateProvinces(DataDateProvince $dataDateProvinces): self
    {
        if (!$this->dataDateProvinces->contains($dataDateProvinces)) {
            $this->dataDateProvinces[] = $dataDateProvinces;
            $dataDateProvinces->setDate($this);
        }

        return $this;
    }

    public function removeDataDateProvinces(DataDateProvince $dataDateProvinces): self
    {
        if ($this->dataDateProvinces->contains($dataDateProvinces)) {
            $this->dataDateProvinces->removeElement($dataDateProvinces);
            // set the owning side to null (unless already changed)
            if ($dataDateProvinces->getDate() === $this) {
                $dataDateProvinces->setDate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateRegion[]
     */
    public function getDataDateRegions(): Collection
    {
        return $this->dataDateRegions;
    }

    public function addDataDateRegion(DataDateRegion $dataDateRegion): self
    {
        if (!$this->dataDateRegions->contains($dataDateRegion)) {
            $this->dataDateRegions[] = $dataDateRegion;
            $dataDateRegion->setDate($this);
        }

        return $this;
    }

    public function removeDataDateRegion(DataDateRegion $dataDateRegion): self
    {
        if ($this->dataDateRegions->contains($dataDateRegion)) {
            $this->dataDateRegions->removeElement($dataDateRegion);
            // set the owning side to null (unless already changed)
            if ($dataDateRegion->getDate() === $this) {
                $dataDateRegion->setDate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateDistrict[]
     */
    public function getDataDateDistricts(): Collection
    {
        return $this->dataDateDistricts;
    }

    public function addDataDateDistrict(DataDateDistrict $dataDateDistrict): self
    {
        if (!$this->dataDateDistricts->contains($dataDateDistrict)) {
            $this->dataDateDistricts[] = $dataDateDistrict;
            $dataDateDistrict->setDate($this);
        }

        return $this;
    }

    public function removeDataDateDistrict(DataDateDistrict $dataDateDistrict): self
    {
        if ($this->dataDateDistricts->contains($dataDateDistrict)) {
            $this->dataDateDistricts->removeElement($dataDateDistrict);
            // set the owning side to null (unless already changed)
            if ($dataDateDistrict->getDate() === $this) {
                $dataDateDistrict->setDate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateSex[]
     */
    public function getDataDateSexes(): Collection
    {
        return $this->dataDateSexes;
    }

    public function addDataDateSex(DataDateSex $dataDateSex): self
    {
        if (!$this->dataDateSexes->contains($dataDateSex)) {
            $this->dataDateSexes[] = $dataDateSex;
            $dataDateSex->setDate($this);
        }

        return $this;
    }

    public function removeDataDateSex(DataDateSex $dataDateSex): self
    {
        if ($this->dataDateSexes->contains($dataDateSex)) {
            $this->dataDateSexes->removeElement($dataDateSex);
            // set the owning side to null (unless already changed)
            if ($dataDateSex->getDate() === $this) {
                $dataDateSex->setDate(null);
            }
        }

        return $this;
    }
}
