<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403205612 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE data_date_district (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, type_case_id INT DEFAULT NULL, number INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_3B6290BB897366B (date_id), INDEX IDX_3B6290BE731BB00 (type_case_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_date_province (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, type_case_id INT DEFAULT NULL, number INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_78ADA987B897366B (date_id), INDEX IDX_78ADA987E731BB00 (type_case_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_date_region (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, type_case_id INT DEFAULT NULL, number INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_141691DFB897366B (date_id), INDEX IDX_141691DFE731BB00 (type_case_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_type (id INT AUTO_INCREMENT NOT NULL, type_case_id INT DEFAULT NULL, number INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_37919CCBE731BB00 (type_case_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data_date_district ADD CONSTRAINT FK_3B6290BB897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_date_district ADD CONSTRAINT FK_3B6290BE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('ALTER TABLE data_date_province ADD CONSTRAINT FK_78ADA987B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_date_province ADD CONSTRAINT FK_78ADA987E731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('ALTER TABLE data_date_region ADD CONSTRAINT FK_141691DFB897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_date_region ADD CONSTRAINT FK_141691DFE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('ALTER TABLE data_type ADD CONSTRAINT FK_37919CCBE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('ALTER TABLE data_date_type RENAME INDEX idx_7f949068b897366b TO IDX_23AF9EEBB897366B');
        $this->addSql('ALTER TABLE data_date_type RENAME INDEX idx_7f949068e731bb00 TO IDX_23AF9EEBE731BB00');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE data_date_district');
        $this->addSql('DROP TABLE data_date_province');
        $this->addSql('DROP TABLE data_date_region');
        $this->addSql('DROP TABLE data_type');
        $this->addSql('ALTER TABLE data_date_type RENAME INDEX idx_23af9eebb897366b TO IDX_7F949068B897366B');
        $this->addSql('ALTER TABLE data_date_type RENAME INDEX idx_23af9eebe731bb00 TO IDX_7F949068E731BB00');
    }
}
