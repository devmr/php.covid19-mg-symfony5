INSERT INTO province (id,name,created_at) SELECT id,libelle,NOW() FROM develections2018.province;
INSERT INTO region (id,name,created_at,province_id) SELECT id,libelle,NOW(),province FROM develections2018.region;
INSERT INTO district (id,name,created_at,province_id,region_id) SELECT a.id,libelle,NOW(),r.province_id,region FROM develections2018.district a INNER JOIN region r ON r.id = a.region ;
