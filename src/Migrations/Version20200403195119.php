<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403195119 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE type_case (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data_all ADD type_case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_all ADD CONSTRAINT FK_7F949068E731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('CREATE INDEX IDX_7F949068E731BB00 ON data_all (type_case_id)');
        $this->addSql('ALTER TABLE data_district ADD type_case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_district ADD CONSTRAINT FK_FB1F9D5BE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('CREATE INDEX IDX_FB1F9D5BE731BB00 ON data_district (type_case_id)');
        $this->addSql('ALTER TABLE data_province ADD type_case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_province ADD CONSTRAINT FK_80041DD7E731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('CREATE INDEX IDX_80041DD7E731BB00 ON data_province (type_case_id)');
        $this->addSql('ALTER TABLE data_region ADD type_case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_region ADD CONSTRAINT FK_6F9CC06FE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('CREATE INDEX IDX_6F9CC06FE731BB00 ON data_region (type_case_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_all DROP FOREIGN KEY FK_7F949068E731BB00');
        $this->addSql('ALTER TABLE data_district DROP FOREIGN KEY FK_FB1F9D5BE731BB00');
        $this->addSql('ALTER TABLE data_province DROP FOREIGN KEY FK_80041DD7E731BB00');
        $this->addSql('ALTER TABLE data_region DROP FOREIGN KEY FK_6F9CC06FE731BB00');
        $this->addSql('DROP TABLE type_case');
        $this->addSql('DROP INDEX IDX_7F949068E731BB00 ON data_all');
        $this->addSql('ALTER TABLE data_all DROP type_case_id');
        $this->addSql('DROP INDEX IDX_FB1F9D5BE731BB00 ON data_district');
        $this->addSql('ALTER TABLE data_district DROP type_case_id');
        $this->addSql('DROP INDEX IDX_80041DD7E731BB00 ON data_province');
        $this->addSql('ALTER TABLE data_province DROP type_case_id');
        $this->addSql('DROP INDEX IDX_6F9CC06FE731BB00 ON data_region');
        $this->addSql('ALTER TABLE data_region DROP type_case_id');
    }
}
