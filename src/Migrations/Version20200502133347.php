<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200502133347 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE confirmedcase_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, disabled_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detail_date_type (id INT AUTO_INCREMENT NOT NULL, type_case_id INT DEFAULT NULL, date_id INT DEFAULT NULL, sexe_id INT DEFAULT NULL, nationality_id INT DEFAULT NULL, confirmedcase_type_id INT DEFAULT NULL, age INT DEFAULT NULL, INDEX IDX_BDF70B68E731BB00 (type_case_id), INDEX IDX_BDF70B68B897366B (date_id), INDEX IDX_BDF70B68448F3B3C (sexe_id), INDEX IDX_BDF70B681C9DA55 (nationality_id), INDEX IDX_BDF70B683F45CB3F (confirmedcase_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nationality (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE detail_date_type ADD CONSTRAINT FK_BDF70B68E731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('ALTER TABLE detail_date_type ADD CONSTRAINT FK_BDF70B68B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE detail_date_type ADD CONSTRAINT FK_BDF70B68448F3B3C FOREIGN KEY (sexe_id) REFERENCES sex (id)');
        $this->addSql('ALTER TABLE detail_date_type ADD CONSTRAINT FK_BDF70B681C9DA55 FOREIGN KEY (nationality_id) REFERENCES nationality (id)');
        $this->addSql('ALTER TABLE detail_date_type ADD CONSTRAINT FK_BDF70B683F45CB3F FOREIGN KEY (confirmedcase_type_id) REFERENCES confirmedcase_type (id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE detail_date_type DROP FOREIGN KEY FK_BDF70B683F45CB3F');
        $this->addSql('ALTER TABLE detail_date_type DROP FOREIGN KEY FK_BDF70B681C9DA55');
        $this->addSql('DROP TABLE confirmedcase_type');
        $this->addSql('DROP TABLE detail_date_type');
        $this->addSql('DROP TABLE nationality');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
