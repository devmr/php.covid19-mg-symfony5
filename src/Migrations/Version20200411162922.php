<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200411162922 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE type_case ADD type_case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE type_case ADD CONSTRAINT FK_AF075C0CE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('CREATE INDEX IDX_AF075C0CE731BB00 ON type_case (type_case_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE type_case DROP FOREIGN KEY FK_AF075C0CE731BB00');
        $this->addSql('DROP INDEX IDX_AF075C0CE731BB00 ON type_case');
        $this->addSql('ALTER TABLE type_case DROP type_case_id');
    }
}
