<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200417085450 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation ADD sex_id INT DEFAULT NULL, ADD type_case_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE translation ADD CONSTRAINT FK_B469456F5A2DB2A0 FOREIGN KEY (sex_id) REFERENCES sex (id)');
        $this->addSql('ALTER TABLE translation ADD CONSTRAINT FK_B469456FE731BB00 FOREIGN KEY (type_case_id) REFERENCES type_case (id)');
        $this->addSql('CREATE INDEX IDX_B469456F5A2DB2A0 ON translation (sex_id)');
        $this->addSql('CREATE INDEX IDX_B469456FE731BB00 ON translation (type_case_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation DROP FOREIGN KEY FK_B469456F5A2DB2A0');
        $this->addSql('ALTER TABLE translation DROP FOREIGN KEY FK_B469456FE731BB00');
        $this->addSql('DROP INDEX IDX_B469456F5A2DB2A0 ON translation');
        $this->addSql('DROP INDEX IDX_B469456FE731BB00 ON translation');
        $this->addSql('ALTER TABLE translation DROP sex_id, DROP type_case_id');
    }
}
