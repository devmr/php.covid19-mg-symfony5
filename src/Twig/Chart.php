<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 09/02/2019
 * Time: 17:00
 */

namespace App\Twig;

use Twig\Extension\RuntimeExtensionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Chart implements RuntimeExtensionInterface
{
    private $arrTypeCharts = [
        'bar',
        'line',
        'pie',
        'scatter',
    ];

    private $translator;



    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function showChart(String $typeChart, $data, $entities, $locale)
    {


        // authorized charts type
        if (!in_array($typeChart, $this->arrTypeCharts) || !isset($data)) {
            return false;
        }

        $str = '';


        // which chart type?
        switch ($typeChart) {
            case 'bar':
                $data = $this->getData($data,$entities,$locale);
                $str = $this->showChartBar($data);
                break;
            case 'line':
                $data = $this->getData($data,$entities,$locale);
                $str = $this->showChartLine($data);
                break;
            case 'pie':
                $data = $this->getPieData($data,$entities,$locale);
                $str = $this->showChartPie($data);
                break;
            case 'scatter':
                $data = $this->getData($data,$entities,$locale);
                $str = $this->showChartScatter($data);
                break;
        }

        return  $str;
    }

    private function showChartLine($data)
    {

        list($arrLabels, $arrValues, $arrColor, $arrDatasetLabels,$arrStrokeWidth,$arrStrokeDash) = $data;

        $arrLabels = array_map(function ($date) {
            return \DateTime::createFromFormat('d/m', $date)->getTimestamp()*1000;
        }, $arrLabels);

        $sAttr = '';
        $sAttr .= ' data-chart';
        $sAttr .= ' data-charts-type="line"' . "\n";
        $sAttr .= ' data-charts-label=\'' . rawurlencode(json_encode($arrLabels)) . '\'' . "\n";
        $sAttr .= ' data-charts-dataset-label="' . rawurlencode(json_encode($arrDatasetLabels)) . '"' . "\n";
        $sAttr .= ' data-charts-data=\'' . json_encode($arrValues) . '\'' . "\n";
        $sAttr .= ' data-charts-backgroundcolor=\'' . json_encode($arrColor) . '\'' . "\n";
        $sAttr .= ' data-charts-bordercolor=\'' . json_encode($arrColor) . '\'' . "\n";
        $sAttr .= ' data-charts-stroke_width=\'' . json_encode($arrStrokeWidth) . '\'' . "\n";
        $sAttr .= ' data-charts-stroke_dash=\'' . json_encode($arrStrokeDash) . '\'' . "\n";


//        $str = '<canvas ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></canvas>'; // Chartjs
        $str = '<div ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></div>'; // ApexCharts
        return $str;
    }

    private function showChartScatter($data)
    {



        list($arrLabels, $arrValues, $arrColor, $arrDatasetLabels,$arrStrokeWidth,$arrStrokeDash) = $data;

        // Contains date d/m format --> convert to timestamp
        $arrLabels = array_map(function ($date) {
            return \DateTime::createFromFormat('d/m', $date)->getTimestamp()*1000;
        }, $arrLabels);

        $sAttr = '';
        $sAttr .= ' data-chart';
        $sAttr .= ' data-charts-type="scatter"' . "\n";
        $sAttr .= ' data-charts-label=\'' . rawurlencode(json_encode($arrLabels)) . '\'' . "\n";
        $sAttr .= ' data-charts-dataset-label="' . rawurlencode(json_encode($arrDatasetLabels)) . '"' . "\n";
        $sAttr .= ' data-charts-data=\'' . json_encode($arrValues) . '\'' . "\n";
        $sAttr .= ' data-charts-backgroundcolor=\'' . json_encode($arrColor) . '\'' . "\n";
        $sAttr .= ' data-charts-bordercolor=\'' . json_encode($arrColor) . '\'' . "\n";
        $sAttr .= ' data-charts-stroke_width=\'' . json_encode($arrStrokeWidth) . '\'' . "\n";
        $sAttr .= ' data-charts-stroke_dash=\'' . json_encode($arrStrokeDash) . '\'' . "\n";


//        $str = '<canvas ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></canvas>'; // Chartjs
        $str = '<div ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></div>'; // ApexCharts
        return $str;
    }
    private function showChartPie($data)
    {

        list($arrLabels, $arrValues, $arrColor, $arrDatasetLabels) = $data;

        $sAttr = '';
        $sAttr .= ' data-chart';
        $sAttr .= ' data-charts-type="pie"' . "\n";
        $sAttr .= ' data-charts-label=\'' . rawurlencode(json_encode($arrLabels)) . '\'' . "\n";
        $sAttr .= ' data-charts-dataset-label="' . rawurlencode(json_encode($arrDatasetLabels)) . '"' . "\n";
        $sAttr .= ' data-charts-data=\'' . json_encode($arrValues) . '\'' . "\n";
        $sAttr .= ' data-charts-backgroundcolor=\'' . json_encode($arrColor) . '\'' . "\n";
        $sAttr .= ' data-charts-bordercolor=\'' . json_encode($arrColor) . '\'' . "\n";


//        $str = '<canvas ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></canvas>'; // Chartjs
        $str = '<div ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></div>'; // ApexCharts
        return $str;
    }
    private function showChartBar($data)
    {

        list($arrLabels, $arrValues, $arrColor, $arrDatasetLabels) = $data;

        $sAttr = '';
        $sAttr .= ' data-chart';
        $sAttr .= ' data-charts-type="bar"' . "\n";
        $sAttr .= ' data-charts-label=\'' . rawurlencode(json_encode($arrLabels)) . '\'' . "\n";
        $sAttr .= ' data-charts-dataset-label="' . rawurlencode(json_encode($arrDatasetLabels)) . '"' . "\n";
        $sAttr .= ' data-charts-data=\'' . json_encode($arrValues) . '\'' . "\n";
        $sAttr .= ' data-charts-backgroundcolor=\'' . json_encode($arrColor) . '\'' . "\n";
        $sAttr .= ' data-charts-bordercolor=\'' . json_encode($arrColor) . '\'' . "\n";


//        $str = '<canvas ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></canvas>'; // Chartjs
        $str = '<div ' . $sAttr . ' class="chartjs" width="undefined" height="undefined"></div>'; // Apexchartj
        return $str;
    }

    private function getPieData($data, $entities, $locale)
    {
        $arrValues = [];
        $arrColor = [];
        $arrLabels = [];
        $arrDatasetLabels = [];
        $keyTmp = null;

        // Array des cles du tableau
        $listKeys = array_keys($data);
        foreach($listKeys as $key) {
            $arrValues[] = $data[$key] ;
            $keyTmp = $key;
        }


        foreach ($entities as $entity) {
            $entityName = $entity->getTranslatedName($locale);
            $entityId = $entity->getId();
            $entityHexaColor = $this->getSettingProperty($entity->getSettings(),'color');

            if (in_array($entityId,$listKeys)) {
                $arrDatasetLabels[] = $entityName;
                $arrColor[] = $entityHexaColor;
            }

        }


        return [$arrLabels,$arrValues,$arrColor,$arrDatasetLabels];
    }
    private function getData($data, $entities, $locale)
    {
        $arrValues = [];
        $arrColor = [];
        $arrStrokeWidth = [];
        $arrStrokeDash = [];
        $keyTmp = null;

        // Array des cles du tableau
        $listKeys = array_keys($data);
        foreach($listKeys as $key) {
            $arrValues[] = $this->flatten($data[$key]) ;

            $keyTmp = $key;
        }

        $arrLabels = array_keys($data[$keyTmp]);

        // Get only Date/month
        $arrLabels = array_map(function ($date) {
            return \DateTime::createFromFormat('Y-m-d', $date)->format('d/m');
        },$arrLabels);


        $arrDatasetLabels = [];
        foreach ($entities as $entity) {
            $entityName = $entity->getTranslatedName($locale);
            $entityId = $entity->getId();
            $entityHexaColor = $this->getSettingProperty($entity->getSettings(),'color');
            $entityStroke = $this->getSettingProperty($entity->getSettings(),'stroke');

            if (in_array($entityId,$listKeys)) {
                $arrDatasetLabels[] = $entityName;
                $arrColor[] = $entityHexaColor;
                $arrStrokeWidth[] = $entityStroke['width'] ?? 0;
                $arrStrokeDash[] = $entityStroke['dash'] ?? 0;
            }
        }


        return [$arrLabels,$arrValues,$arrColor,$arrDatasetLabels,$arrStrokeWidth,$arrStrokeDash];
    }

    /**
     * Flattens the given `$array`, excluding all `null` items.
     *
     * @param array $array
     *
     * @return array
     */
    private function flatten(array $array): array
    {

        $result = [];


        foreach ($array as $item) {
            if (is_null($item)) {
                continue;
            }

            $result = array_merge($result, is_array($item) ? self::flatten($item) : [$item]);
        }

        return array_values($result);
    }

    private function getSettingProperty($settings, string $string)
    {
        if ($string === '') {
            return false;
        }

        $list = json_decode($settings, true);


        return $list['charts'][$string] ?? '';
    }
}
