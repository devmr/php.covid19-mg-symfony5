<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [

            // All map
            new TwigFunction('showMap',
                [MapSvg::class, 'showMap'],
                ['is_safe' => ['html']]
            ),

            // Chart js
            new TwigFunction('showChart',
                [Chart::class, 'showChart'],
                ['is_safe' => ['html']]
            ),
        ];
    }

}
