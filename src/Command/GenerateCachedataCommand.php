<?php

namespace App\Command;

use App\Entity\Date;
use App\Entity\Province;
use App\Services\CacheData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateCachedataCommand extends Command
{
    protected static $defaultName = 'app:generate-cachedata';

    private $argList = ['district', 'svg'];

    private $em;
    private $cacheData;


    public function __construct(EntityManagerInterface $em, CacheData $cacheData)
    {
        $this->em = $em;
        $this->cacheData = $cacheData;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate svg data / data by district and store into cache')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'district or svg')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if (!$arg1 || !in_array($arg1, $this->argList)) {
            $io->error('Argument is empty or not in list');
            return 1;
        }

        switch ($arg1) {
            case 'district':
                $this->generateCachedataDistrict($input, $output);
                break;

            case 'svg':
            default:
                $this->generateCachedataSvg($input, $output);
                break;
        }




        return 0;
    }

    private function generateCachedataDistrict(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Generate cache data district');

        $provinces = $this->em->getRepository(Province::class)->findAll();
        foreach ($provinces as $province) {
            $provinceId = $province->getId();
            $provinceName = $province->getName();

            $io->note('Cache data district - Province: ' . $provinceName);

            $this->cacheData->getCacheDataDistrictsByProvince($provinceId);
        }

        $io->success('Cached generated successfully for district.');

    }

    private function generateCachedataSvg(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('Generate cache data SVG');

        $provinces = $this->em->getRepository(Province::class)->findAll();
        foreach ($provinces as $province) {
            $provinceId = $province->getId();
            $provinceName = $province->getName();

            $io->note('Cache data SVG - Province: ' . $provinceName);

            $this->cacheData->getCacheSvgDailyDataProvince($provinceId);
        }

        $io->success('Cached generated successfully for svg.');
    }
}
