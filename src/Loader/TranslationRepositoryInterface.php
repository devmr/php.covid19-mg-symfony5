<?php


namespace App\Loader;

use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ObjectRepository;

interface TranslationRepositoryInterface extends ObjectRepository
{
    /**
     * @param string $domain
     * @param string $locale
     *
     * @return array|Collection|EntityInterface[]
     */
    public function findByDomainAndLocale(string $domain, string $locale);
}
