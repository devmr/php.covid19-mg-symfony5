<?php

namespace App\Controller;

use App\Entity\DataDateProvince;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class FrontendAjaxController extends AbstractController
{
    /**
     * @Route("/draftpage", name="frontend_draftpage")
     */
    public function draftpage()
    {

        

        return new Response('<body>Bonjour </body>');
    }

    public function dataDayEchelle($twigfile)
    {
        // all confirmed cases by Dates
        $confirmedCases = $this->getDoctrine()->getRepository(DataDateProvince::class)->findConfirmedCasesOrderByDate('ASC');

        $data = $this->getData($confirmedCases);


        $xyCoordList = [
            // 1 to 9
            1 => [
              40 => [34.018013,144.73788],
              60 => [45.082005,144.73788],
              90 => [45.082005,144.73788],
              100 => [66.507439,144.73788],
            ],

            // 10 to 99
            2 => [
                40 => [32.959679,144.73788],
                60 => [44.02367,144.73788],
                90 => [54.665329,144.73788],
                100 => [65.978271,144.73788],
            ],

            // 100 to 999
            3 => [
                40 => [31.901344,144.73788],
                60 => [42.965336,144.73788],
                90 => [53.935707,144.73788],
                100 => [64.919937,144.73788],
            ],
        ];

        list($firstDate,$lastDate,$total,$maxNumber,$confirmedCasesCount,$listStylesPercent,$echelleList) = $data;

        $data = [];
        foreach ($echelleList as $key => $value) {

            // ignore 0 && 20% --> start to 40%
            if (in_array($key, [0,20])) {
                continue;
            }

            $data[] = [
                'value' => $value,
                'coordX' => $xyCoordList[strlen($value)][$key][0] ?? 0,
                'coordY' => $xyCoordList[strlen($value)][$key][1] ?? 0,
            ];
        }

        return $this->render($twigfile, ['echelle'=>$data, 'firstDate'=>$firstDate, 'lastDate'=>$lastDate]);
    }

    private function getStylesPercentage($number)
    {
        switch (true) {
            case $number == 0:
                return 'percent-0';

            case $number>0 && $number<20:
                return 'percent-10';

            case $number>=20 && $number<40:
                return 'percent-20';

            case $number>=40 && $number<60:
                return 'percent-40';

            case $number>=60 && $number<90:
                return 'percent-60';

            default:
                return 'percent-100';
        }
    }

    private function getData($confirmedCases, $maxNumber=0)
    {
        $confirmedCasesCount = count($confirmedCases);

        $listDate = [];
        $listNumber = [];
        $i = 0;
        $total = 0;
        $firstDate = '';
        $lastDate = '';
        foreach ($confirmedCases as $row) {

            // number
            $number = $row->getNumber();
            $listNumber[] = $number;

            // Total
            $total += $number;

            // First Date
            if ($i == 0) {
                $firstDate = $row->getDate()->getDate();
            }

            // Last Date
            if ($i+1 == $confirmedCasesCount) {
                $lastDate = $row->getDate()->getDate();
            }

            $listDate[] = $row->getDate()->getDate();

            $i++;
        }

        // max number
        $maxNumber = $maxNumber>0 ?  $maxNumber : max($listNumber);


        // number + percent
        $listStylesPercent = [];
        foreach ($listNumber as $number) {
            $percent = $maxNumber>0 ? round(($number/$maxNumber)*100) : 0;
            $stylePercent = $this->getStylesPercentage($percent);

//            $listNumberTmp[] = [$number, $percent, $stylePercent];
            $listStylesPercent[] = $stylePercent;
        }

        // Echelle
        $echelleList[0] = 0;
        foreach ([20,40,60,90] as $percent) {
            $targetNumber = intval(floor(($percent*$maxNumber)/100));
            $echelleList[$percent] = $targetNumber;
        }
        $echelleList[100] = $maxNumber;


        return [ $firstDate,$lastDate,$total,$maxNumber,$confirmedCasesCount, $listStylesPercent, $echelleList, $listDate];
    }
}
