<?php

namespace App\Controller\ApiPlatform;

use App\Entity\District;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateDistrict extends AbstractController
{

    public function __invoke(District $data): District
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name . ' (copy)');

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
