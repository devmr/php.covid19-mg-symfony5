<?php

namespace App\Controller\ApiPlatform;

use App\Entity\DataDateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateDataDateType extends AbstractController
{

    public function __invoke(DataDateType $data): DataDateType
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name );

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
