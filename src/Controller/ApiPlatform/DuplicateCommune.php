<?php

namespace App\Controller\ApiPlatform;

use App\Entity\Commune;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateCommune extends AbstractController
{

    public function __invoke(Commune $data): Commune
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name . ' (copy)');

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
