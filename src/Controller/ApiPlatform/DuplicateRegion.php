<?php

namespace App\Controller\ApiPlatform;

use App\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateRegion extends AbstractController
{

    public function __invoke(Region $data): Region
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name . ' (copy)');

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
