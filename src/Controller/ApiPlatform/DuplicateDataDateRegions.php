<?php

namespace App\Controller\ApiPlatform;

use App\Entity\DataDateRegion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateDataDateRegions extends AbstractController
{

    public function __invoke(DataDateRegion $data): DataDateRegion
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name );

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
