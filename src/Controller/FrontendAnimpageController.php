<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontendAnimpageController extends AbstractController
{
    /**
     * @Route({
     *  "fr": "/animation",
     *  "mg": "/mg/animation",
     *  "en": "/en/animation"
     * }, name="frontend_animation")
     */
    public function index()
    {
        return $this->render('frontend/animpage/index.html.twig', []);
    }
}
