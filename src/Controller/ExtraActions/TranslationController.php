<?php

namespace App\Controller\ExtraActions;

use App\Entity\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TranslationController extends AbstractController
{
    /**
     * @Route("/api/translations/edit/{ids}", name="api_translation_getmultiple")
     */
    public function getMultiple($ids, SerializerInterface $serializer)
    {
        $rows = $this->getDoctrine()->getRepository(Translation::class)->findByIds($ids);
        $resultat = $serializer->serialize(
            ['hydra:member' => $rows],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

    /**
     * @Route("/api/translations/update-all", name="api_translations_updateall")
     */
    public function updateAll(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        if (empty($dataTab)) {
            return new JsonResponse([],200,[],true);
        }

        if (isset($dataTab['updatedAt'])) {
            $dataTab['updatedAt'] = (new \DateTime($dataTab['updatedAt']))->format('Y-m-d H:i:s');
        }

        $repo = $this->getDoctrine()->getRepository(Translation::class);
        $sql = $repo->updateAll($dataTab);

        $result = ['OK'];

        //cd
        $resultat = $serializer->serialize(
            ['hydra:member' => $result],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }
}
