<?php

namespace App\Controller\ExtraActions;

use App\Entity\DataDateType;
use App\Entity\TypeCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DataDateTypeController extends AbstractController
{
    /**
     * @Route("/api/data_date_types/edit/{ids}", name="api_data_date_types_getmultiple")
     */
    public function getMultiple($ids, SerializerInterface $serializer)
    {
        $rows = $this->getDoctrine()->getRepository(DataDateType::class)->findByIds($ids);
        $resultat = $serializer->serialize(
            ['hydra:member' => $rows],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

    /**
     * @Route("/api/data_date_types/charts", name="api_data_date_types_charts")
     */
    public function getDataChart(SerializerInterface $serializer)
    {
        // Result by date and type
        $resultByDateAndType = $this->getDoctrine()->getRepository(DataDateType::class)->findAllOrderByDate('ASC');

        // Data by date and by Case Types
        $dataDateType = [];
        $dataDateTypeNumberAdded = [];
        foreach ($resultByDateAndType as $result) {
            $date = $result->getDate()->getDate()->format('Y-m-d');
            $number = $result->getNumber();
            $caseTypeId = $result->getTypeCase()->getId();

            // 1 / 2 / 3 - Confirmed cases / Recovered / Death
            if (in_array($caseTypeId, [1,2,3])) {
                $dataDateType[$caseTypeId][$date] = $number;
            }

            // 2 / 5 / 6 - recovered, imported case, local cases
            if (in_array($caseTypeId, [2, 5, 6])) {
                $dataDateTypeNumberAdded[$caseTypeId][$date] = $result->getNumberLastAdded();
            }
        }

        // Type cases entities
        $typeCaseEntities = $this->getDoctrine()->getRepository(TypeCase::class)->findAllOrderedBy('position');

        $data = [
            'data' => $dataDateType,
            'entities' => $typeCaseEntities
        ];

        $resultat = $serializer->serialize(
            ['hydra:member' => $data],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);


    }

    /**
     * @Route("/api/data_date_types/update-all", name="api_data_date_types_updateall")
     */
    public function updateAll(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        if (empty($dataTab)) {
            return new JsonResponse([],200,[],true);
        }

        if (isset($dataTab['updatedAt'])) {
            $dataTab['updatedAt'] = (new \DateTime($dataTab['updatedAt']))->format('Y-m-d H:i:s');
        }

        $repo = $this->getDoctrine()->getRepository(DataDateType::class);
        $sql = $repo->updateAll($dataTab);

        $result = ['OK'];

        //cd
        $resultat = $serializer->serialize(
            ['hydra:member' => $result],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

}
