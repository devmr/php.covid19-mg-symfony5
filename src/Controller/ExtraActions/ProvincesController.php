<?php

namespace App\Controller\ExtraActions;

use App\Entity\Province;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProvincesController extends AbstractController
{
    /**
     * @Route("/api/provinces/edit/{ids}", name="api_provinces_getmultiple")
     */
    public function getMultiple($ids, SerializerInterface $serializer)
    {
        $rows = $this->getDoctrine()->getRepository(Province::class)->findByIds($ids);
        $resultat = $serializer->serialize(
            ['hydra:member' => $rows],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

    /**
     * @Route("/api/provinces/update-all", name="api_provinces_updateall")
     */
    public function updateAll(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        if (empty($dataTab)) {
            return new JsonResponse([],200,[],true);
        }

        if (isset($dataTab['updatedAt'])) {
            $dataTab['updatedAt'] = (new \DateTime($dataTab['updatedAt']))->format('Y-m-d H:i:s');
        }

        $repo = $this->getDoctrine()->getRepository(Province::class);
        $sql = $repo->updateAll($dataTab);

        $result = ['OK'];

        //cd
        $resultat = $serializer->serialize(
            ['hydra:member' => $result],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

}
