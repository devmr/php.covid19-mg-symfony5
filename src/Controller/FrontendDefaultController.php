<?php

namespace App\Controller;

use App\Entity\DataDateType;
use App\Entity\DataDistrict;
use App\Entity\DataSex;
use App\Entity\DataType;
use App\Entity\District;
use App\Entity\Province;
use App\Entity\Sex;
use App\Entity\TypeCase;
use App\Repository\DataDateTypeRepository;
use App\Repository\DataProvinceRepository;
use App\Repository\DataTypeRepository;
use App\Repository\ProvinceRepository;
use App\Repository\TypeCaseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class FrontendDefaultController extends AbstractController
{
    /**
     * @Route({
     *  "fr": "/",
     *  "mg": "/mg",
     *  "en": "/en"
     * }, name="frontend_default")
     * @param DataTypeRepository $dataTypeRepository
     * @param TypeCaseRepository $typeCaseRepository
     * @param DataDateTypeRepository $dataDateTypeRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, TypeCaseRepository $typeCaseRepository, DataTypeRepository $dataTypeRepository, CacheInterface $cache)
    {

        // Result by date and type
        $resultByDateAndType = $this->getDoctrine()->getRepository(DataDateType::class)->findAllOrderByDate('ASC');

        // Data by date and by Case Types
        $dataDateType = [];
        $dataDateTypeNumberAdded = [];
        foreach ($resultByDateAndType as $result) {
            $date = $result->getDate()->getDate()->format('Y-m-d');
            $number = $result->getNumber();
            $caseTypeId = $result->getTypeCase()->getId();

            // 1 / 2 / 3 - Confirmed cases / Recovered / Death
            if (in_array($caseTypeId, [1,2,3])) {
                $dataDateType[$caseTypeId][$date] = $number;
            }

            // 2 / 5 / 6 - recovered, imported case, local cases
            if (in_array($caseTypeId, [2, 5, 6])) {
                $dataDateTypeNumberAdded[$caseTypeId][$date] = $result->getNumberLastAdded();
            }
        }



        // Type cases entities
        $typeCaseEntities = $this->getDoctrine()->getRepository(TypeCase::class)->findAllOrderedBy('position');

        // Last updated date
        $entityDataType = $this->getDoctrine()->getRepository(DataType::class)->findAll();

        // $provinces
        $provinces = $this->getDoctrine()->getRepository(Province::class)->findAll();

        $listCases = $this->getAllCases($typeCaseEntities, $entityDataType, $request);

        return $this->render('frontend/default/index.html.twig', [
            'dataDateType' => $dataDateType,
            'entityDataType'=>$entityDataType,
            'listCases'=>$listCases,
            'typeCaseEntities' => $typeCaseEntities,
            'dataDateTypeNumberAdded' => $dataDateTypeNumberAdded,
            'provinces' => $provinces,
        ]);
    }

    /**
     * Daily confirmed cases by provinces
     *
     * @param $type
     * @param $twigfile
     * @param ProvinceRepository $provinceRepository
     * @param DataProvinceRepository $dataProvincesRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dataProvinces($type, $twigfile, ProvinceRepository $provinceRepository, DataProvinceRepository $dataProvincesRepository, CacheInterface $cache)
    {
        // DataProvinces From DB
        $provinceEntity = $provinceRepository->findAll();
        $dataProvincesEntity = $dataProvincesRepository->findBy(['disabledAt' => null, 'typeCase' => 1]);

        // DataProvinces From DB
        $dataDistrictsEntity = $this->getDoctrine()->getRepository(DataDistrict::class)->findBy(['disabledAt' => null, 'typeCase' => 1]);

        // All numbers from DataProvinces
        $listDigits = [];
        $listDigitsLastAdded = [];
        $listLastAddedUpdatedAt = [];
        foreach ($dataProvincesEntity as $dataType) {

            $dataProvinceId = $dataType->getProvince()->getId();
            $datatypeNumber = $dataType->getNumber();
            $datatypeNumberLastAdded = $dataType->getNumberLastAdded();
            $datatypeUpdatedAt = $dataType->getUpdatedAt();

            $listDigits[$dataProvinceId] = $datatypeNumber;
            $listDigitsLastAdded[$dataProvinceId] = $datatypeNumberLastAdded;
            $listLastAddedUpdatedAt[$dataProvinceId] = $datatypeUpdatedAt ?? '';
        }

        // All numbers from DataDistricts
        $listDistrictsDigits = [];
        foreach ($dataDistrictsEntity as $dataType) {

            $dataDistrictId = $dataType->getDistrict()->getId();
            $datatypeNumber = $dataType->getNumber();

            $listDistrictsDigits[$dataDistrictId] = $datatypeNumber;
        }

        // All type cases
        $listCases = [];
        $countUpdated = 0;
        foreach ($provinceEntity as $province) {

            $provinceId = $province->getId();
            $provinceName = $province->getName();
            $districts = $province->getDistricts();
            $caseCountLastAdded = ($listDigitsLastAdded[$provinceId] ?? 0);
            $lastAddedUpdatedAt = $listLastAddedUpdatedAt[$provinceId] ?? '';

            $svgCached = $cache->get('dataprovince_' . $provinceId, function (ItemInterface $item) {
                return;
            });

            $districtsprovince = $cache->get('districtsprovince_' . $provinceId, function (ItemInterface $item) {
                return [];
            });



            $listCases[$provinceId]['id'] = $provinceId;
            $listCases[$provinceId]['name'] = $provinceName;
            $listCases[$provinceId]['number'] = ($listDigits[$provinceId] ?? 0);
            $listCases[$provinceId]['numberLastAdded'] = $caseCountLastAdded;
            $listCases[$provinceId]['lastAddedUpdatedAt'] = $lastAddedUpdatedAt;
            $listCases[$provinceId]['percent'] = 0;
            $listCases[$provinceId]['districts'] = $this->mappingDataDistrictsProvince($districtsprovince, $listDistrictsDigits);
            $listCases[$provinceId]['svg'] = $svgCached;

            if ($caseCountLastAdded>0) {
                $countUpdated++;
            }
        }

        return $this->render($twigfile, [
            'type' => $type,
            'lists' => $listCases,
            'lastAddedUpdatedAt' => $lastAddedUpdatedAt,
            'countUpdated' => $countUpdated,
        ]);
    }

    private function getAllCases($typeCaseEntity, $dataTypeEntity,  Request $request)
    {
        // Langue locale
        $locale = $request->getLocale();

        // All numbers
        $listDigits = [];
        $listDigitsLastAdded = [];
        $listLastAddedUpdatedAt = [];
        $caseTotalCount = 0;
        foreach ($dataTypeEntity as $dataType) {

            $datatypeCaseId = $dataType->getTypeCase()->getId();
            $datatypeNumber = $dataType->getNumber();
            $datatypeNumberLastAdded = $dataType->getNumberLastAdded();
            $datatypeUpdatedAt = $dataType->getUpdatedAt();

            // Cas 1 = confirmed
            if ($datatypeCaseId == 1) {
                $caseTotalCount = $datatypeNumber ?? 0;
            }

            $listDigits[$datatypeCaseId] = $datatypeNumber;
            $listDigitsLastAdded[$datatypeCaseId] = $datatypeNumberLastAdded;
            $listLastAddedUpdatedAt[$datatypeCaseId] = $datatypeUpdatedAt;
        }

        // All type cases
        $listCases = [];
        foreach ($typeCaseEntity as $typeCase) {

            $typeCaseId = $typeCase->getId();
            $typeCaseName = $typeCase->getTranslatedName($locale);
            $caseCount = ($listDigits[$typeCaseId] ?? 0);
            $caseCountLastAdded = ($listDigitsLastAdded[$typeCaseId] ?? 0);
            $lastAddedUpdatedAt = $listLastAddedUpdatedAt[$typeCaseId] ?? '';
            $percent = round( ($caseCount/$caseTotalCount)*100, 2);

            $listCases[$typeCaseId]['name'] = $typeCaseName;
            $listCases[$typeCaseId]['number'] = $caseCount;
            $listCases[$typeCaseId]['numberLastAdded'] = $caseCountLastAdded;
            $listCases[$typeCaseId]['lastAddedUpdatedAt'] = $lastAddedUpdatedAt;
            $listCases[$typeCaseId]['percent'] = $percent;

        }

        return $listCases;
    }

    private function mappingDataDistrictsProvince($districts, array $listDistrictsDigits)
    {
        $data = [];
        foreach($districts as $district) {
            $name = $district->getName();
            $id = $district->getId();

            if (in_array($id, array_keys($listDistrictsDigits))) {
                $data[] = [$name, $listDistrictsDigits[$id]];
            }
        }

        return $data;
    }
}
