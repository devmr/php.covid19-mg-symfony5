<?php

namespace App\EventListener;

use App\Entity\DataDateProvince;
use App\Entity\DataProvince;
use App\Entity\Date;
use App\Services\CacheData;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class DataProvincesUpdater
{
    /**
     * @var EntityManager
     */
    private $em;

    private $cacheData;

    public function __construct(EntityManagerInterface $em, CacheData $cacheData)
    {
        $this->em = $em;
        $this->cacheData = $cacheData;
    }

    public function postUpdate(DataProvince $dataProvince, LifecycleEventArgs $event)
    {
        $updatedAt = $dataProvince->getUpdatedAt();

        // Id of date
        $entityDate = $event->getObjectManager()->getRepository(Date::class)->findOneByDate($updatedAt);
        $dateId = $this->createOrNullDate($entityDate, $updatedAt);

        // Add or Update data_date_province
        $this->createOrNullDataDateProvince($dataProvince, $dateId);

        // Create cache Data
        $provinceId = $dataProvince->getProvince()->getId();
        $this->cacheData->getCacheSvgDailyDataProvince($provinceId);


    }

    private function createOrNullDate($entityDate, $updatedAt)
    {

        if ($entityDate !== null) {
            return $entityDate;
        }

        // otherwise create Date
        $entityDate = new Date();
        $entityDate->setDate($updatedAt);

        $this->em->persist($entityDate);
        $this->em->flush();

        return $entityDate;
    }

    private function createOrNullDataDateProvince(DataProvince $dataProvince, Date $date)
    {

        $entityDataDateProvince = $this->em->getRepository(DataDateProvince::class)->findOneBy(
            [
                'date' => $date->getId(),
                'province'=>$dataProvince->getProvince()->getId(),
                'typeCase'=>$dataProvince->getTypeCase()->getId()
            ]
        );

        $exists = true;
        if ($entityDataDateProvince === null) {
            // Create
            $entityDataDateProvince = new DataDateProvince();
            $exists = false;
        }

        $entityDataDateProvince->setDate($date);
        $entityDataDateProvince->setNumber($dataProvince->getNumberLastAdded());
        $entityDataDateProvince->setTypeCase($dataProvince->getTypeCase());
        $entityDataDateProvince->setProvince($dataProvince->getProvince());

        if (!$exists) {
            $this->em->persist($entityDataDateProvince);
        }

        // Flush
        $this->em->flush();


    }
}
