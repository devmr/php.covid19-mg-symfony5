<?php

namespace App\EventListener;

use App\Entity\TypeCase;
use App\Entity\Date;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class TypeCaseUpdater
{

    public function prePersist(TypeCase $typeCase, LifecycleEventArgs $event)
    {

        // Get max position
        $entity = $event->getObjectManager()->getRepository(TypeCase::class)->findMaxPosition();

        // Set position
        $typeCase->setPosition(intval(intval($entity)+1));


    }


}
