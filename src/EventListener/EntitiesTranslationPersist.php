<?php

namespace App\EventListener;

use App\Kernel;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class EntitiesTranslationPersist
{
    var $kernel;
    var $fs;

    public function __construct(KernelInterface $kernel, Filesystem $fs)
    {
        $this->kernel = $kernel;
        $this->fs = $fs;
    }

    /**
     * FILE = 2 / ENTITY = 1
     */
    const ENTITY_TYPE = 1;

    /**
     * After Created
     *
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {

        $update = false;

        $entity = $args->getObject();

        $class = get_class($entity);
        switch ($class) {
            case "App\Entity\Translation":
                $update = true;
                break;
        }

        if (!$update) {
            return;
        }

        // entity ID
        $translationTypeId = $entity->getTranslationType()->getId();

        // Do only if type is entity
        if (self::ENTITY_TYPE !== $translationTypeId) {
            // Remove all files inside /var/cache/translations
            $this->clearCacheTranslations($entity->getLocale());
            return;
        }

        $serializer = new CamelCaseToSnakeCaseNameConverter();


        // Entity to translate
        $keyTranslate = explode('.', $entity->getKeytranslate())[1];
        // Lang
        $locale = $entity->getLocale();
        // Keyword
        $keyword = $entity->getTranslation();

        // Convert to CamelCase
        $keyTranslate = ucfirst($serializer->denormalize($keyTranslate));


        $className = "App\Entity\\$keyTranslate";


        if (!class_exists($className)) {
            return;
        }

        // create
        $entityDb = new $className();
        $entityDb->translate($locale)->setName($keyword);

        if ($keyTranslate === 'Sex') {
            $entity->addSex($entityDb);
        } else {
            $entity->addTypeCase($entityDb);
        }

        // Update
        // Persist and flush
        $args->getObjectManager()->persist($entityDb);

        // In order to persist new translations, call mergeNewTranslations method, before flush
        $entityDb->mergeNewTranslations();

        $args->getObjectManager()->persist($entity);
        $args->getObjectManager()->flush();


    }

    /**
     * After Updated
     *
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {

        $update = false;

        $entity = $args->getObject();


        $class = get_class($entity);
        switch ($class) {
            case "App\Entity\Translation":
                $update = true;
                break;
        }

        if (!$update) {
            return;
        }

        // entity ID
        $translationTypeId = $entity->getTranslationType()->getId();

        // Do only if type is entity
        if (self::ENTITY_TYPE !== $translationTypeId) {
            // Remove all files inside /var/cache/translations
            $this->clearCacheTranslations($entity->getLocale());
            return;
        }

        // Entity to translate
        $sexes = $entity->getSexes();
        $typecases = $entity->getTypeCases();

        // Lang
        $locale = $entity->getLocale();

        // Keyword
        $keyword = $entity->getTranslation();

        // If sexe is translated
        if ($sexes->count()>0) {

            foreach ($sexes as $entityTranslated) {
                // Update translated
                $entityTranslated->getTranslations()->get($locale)->setName($keyword);
            }
        }


        // If typecases is translated
        if ($typecases->count()>0) {
            foreach ($typecases as $entityTranslated) {
                // Update translated
                $entityTranslated->getTranslations()->get($locale)->setName($keyword);
            }
        }

        // Persist and flush
        $args->getObjectManager()->persist($entity);
        $args->getObjectManager()->flush();
    }

    /**
     * Remove all files inside /var/cache/translations
     *
     * @param KernelInterface $kernel
     * @param Filesystem $filesystem
     */
    private function clearCacheTranslations($locale)
    {

        $realCacheDir = $this->kernel->getContainer()->getParameter('kernel.cache_dir');
        foreach (glob($realCacheDir .'/translations/catalogue.' . $locale . '*') as $file) {
            $this->fs->remove($file);
        }
        return;
    }

}
