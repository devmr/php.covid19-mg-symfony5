<?php

namespace App\EventListener;

use App\Entity\DataDateRegion;
use App\Entity\DataRegion;
use App\Entity\Date;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class DataRegionsUpdater
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function postPersist(DataRegion $dataProvince, LifecycleEventArgs $event)
    {
        $updatedAt = $dataProvince->getUpdatedAt();

        // Id of date
        $entityDate = $event->getObjectManager()->getRepository(Date::class)->findOneByDate($updatedAt);
        $dateId = $this->createOrNullDate($entityDate, $updatedAt);

        // Add or Update data_date_province
        $this->createOrNullDataDateRegion($dataProvince, $dateId);

    }

    public function postUpdate(DataRegion $dataProvince, LifecycleEventArgs $event)
    {
        $updatedAt = $dataProvince->getUpdatedAt();

        // Id of date
        $entityDate = $event->getObjectManager()->getRepository(Date::class)->findOneByDate($updatedAt);
        $dateId = $this->createOrNullDate($entityDate, $updatedAt);

        // Add or Update data_date_province
        $this->createOrNullDataDateRegion($dataProvince, $dateId);


    }

    private function createOrNullDate($entityDate, $updatedAt)
    {

        if ($entityDate !== null) {
            return $entityDate;
        }

        // otherwise create Date
        $entityDate = new Date();
        $entityDate->setDate($updatedAt);

        $this->em->persist($entityDate);
        $this->em->flush();

        return $entityDate;
    }

    private function createOrNullDataDateRegion(DataRegion $dataProvince, Date $date)
    {

        $entityDataDateRegion = $this->em->getRepository(DataDateRegion::class)->findOneBy(
            [
                'date' => $date->getId(),
                'region'=>$dataProvince->getRegion()->getId(),
                'typeCase'=>$dataProvince->getTypeCase()->getId()
            ]
        );

        $exists = true;
        if ($entityDataDateRegion === null) {
            // Create
            $entityDataDateRegion = new DataDateRegion();
            $exists = false;
        }

        $entityDataDateRegion->setDate($date);
        $entityDataDateRegion->setNumber($dataProvince->getNumberLastAdded());
        $entityDataDateRegion->setTypeCase($dataProvince->getTypeCase());
        $entityDataDateRegion->setRegion($dataProvince->getRegion());

        if (!$exists) {
            $this->em->persist($entityDataDateRegion);
        }

        // Flush
        $this->em->flush();


    }
}
