<?php

namespace App\EventListener;

use App\Entity\DataDateDistrict;
use App\Entity\DataDistrict;
use App\Entity\Date;
use App\Services\CacheData;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class DataDistrictsUpdater
{
    /**
     * @var EntityManager
     */
    private $em;
    private $cacheData;

    public function __construct(EntityManagerInterface $em, CacheData $cacheData)
    {
        $this->em = $em;
        $this->cacheData = $cacheData;
    }

    public function postUpdate(DataDistrict $dataDistrict, LifecycleEventArgs $event)
    {
        $updatedAt = $dataDistrict->getUpdatedAt();

        // Id of date
        $entityDate = $event->getObjectManager()->getRepository(Date::class)->findOneByDate($updatedAt);
        $dateId = $this->createOrNullDate($entityDate, $updatedAt);

        // Add or Update data_date_province
        $this->createOrNullDataDateDistrict($dataDistrict, $dateId);

        // Create cache Data
        $provinceId = $dataDistrict->getDistrict()->getProvince()->getId();
        $this->cacheData->getCacheDataDistrictsByProvince($provinceId);


    }

    public function postPersist(DataDistrict $dataDistrict, LifecycleEventArgs $event)
    {
        $updatedAt = $dataDistrict->getUpdatedAt();

        // Id of date
        $entityDate = $event->getObjectManager()->getRepository(Date::class)->findOneByDate($updatedAt);
        $dateId = $this->createOrNullDate($entityDate, $updatedAt);

        // Add or Update data_date_province
        $this->createOrNullDataDateDistrict($dataDistrict, $dateId);

        // Create cache Data
        $provinceId = $dataDistrict->getDistrict()->getProvince()->getId();
        $this->cacheData->getCacheDataDistrictsByProvince($provinceId);


    }

    private function createOrNullDate($entityDate, $updatedAt)
    {

        if ($entityDate !== null) {
            return $entityDate;
        }

        // otherwise create Date
        $entityDate = new Date();
        $entityDate->setDate($updatedAt);

        $this->em->persist($entityDate);
        $this->em->flush();

        return $entityDate;
    }

    private function createOrNullDataDateDistrict(DataDistrict $dataProvince, Date $date)
    {

        $entityDataDateDistrict = $this->em->getRepository(DataDateDistrict::class)->findOneBy(
            [
                'date' => $date->getId(),
                'district'=>$dataProvince->getDistrict()->getId(),
                'typeCase'=>$dataProvince->getTypeCase()->getId()
            ]
        );

        $exists = true;
        if ($entityDataDateDistrict === null) {
            // Create
            $entityDataDateDistrict = new DataDateDistrict();
            $exists = false;
        }

        $entityDataDateDistrict->setDate($date);
        $entityDataDateDistrict->setNumber($dataProvince->getNumberLastAdded());
        $entityDataDateDistrict->setTypeCase($dataProvince->getTypeCase());
        $entityDataDateDistrict->setDistrict($dataProvince->getDistrict());

        if (!$exists) {
            $this->em->persist($entityDataDateDistrict);
        }

        // Flush
        $this->em->flush();


    }
}
