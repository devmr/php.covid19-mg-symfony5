<?php

namespace App\EventListener;

use App\Entity\DataDateType;
use App\Entity\DataType;
use App\Entity\Date;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class DataTypesUpdater
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function postUpdate(DataType $dataProvince, LifecycleEventArgs $event)
    {
        $updatedAt = $dataProvince->getUpdatedAt();

        // Id of date
        $entityDate = $event->getObjectManager()->getRepository(Date::class)->findOneByDate($updatedAt);
        $dateId = $this->createOrNullDate($entityDate, $updatedAt);

        // Add or Update data_date_province
        $this->createOrNullDataDateType($dataProvince, $dateId);


    }

    private function createOrNullDate($entityDate, $updatedAt)
    {

        if ($entityDate !== null) {
            return $entityDate;
        }

        // otherwise create Date
        $entityDate = new Date();
        $entityDate->setDate($updatedAt);

        $this->em->persist($entityDate);
        $this->em->flush();

        return $entityDate;
    }

    private function createOrNullDataDateType(DataType $dataProvince, Date $date)
    {

        $entityDataDateType = $this->em->getRepository(DataDateType::class)->findOneBy(
            ['date' => $date->getId(), 'typeCase'=>$dataProvince->getTypeCase()->getId()]
        );

        $exists = true;
        if ($entityDataDateType === null) {
            // Create
            $entityDataDateType = new DataDateType();
            $exists = false;
        }

        $entityDataDateType->setDate($date);
        $entityDataDateType->setNumber($dataProvince->getNumber());
        $entityDataDateType->setNumberLastAdded($dataProvince->getNumberLastAdded());
        $entityDataDateType->setTypeCase($dataProvince->getTypeCase());

        if (!$exists) {
            $this->em->persist($entityDataDateType);
        }

        // Flush
        $this->em->flush();


    }
}
