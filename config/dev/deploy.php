<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            // SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
            ->server('madatsara')
            // the absolute path of the remote server directory where the project is deployed
            ->deployDir('/home/web/covid19mg/dev')
            // the URL of the Git repository where the project code is hosted
            ->repositoryUrl('git@gitlab.com:devmr/covid19-mg.git')
            // the repository branch to deploy
            ->repositoryBranch('dev')
            // avoid error: Script @auto-scripts was called via post-install-cmd
            ->composerInstallFlags('--prefer-dist --no-interaction --no-dev')
            // avoid PHP Fatal error:  Uncaught Symfony\Component\Dotenv\Exception\PathException: Unable to read the
            ->sharedFilesAndDirs(['.env', '.env.local', 'config/jwt/'])
            // Releases folder to keep
            ->keepReleases(1)
            ;
    }

    public function beforePublishing()
    {
        $this->log('<h1>chown -R web: /releases</>');
        // $this->runRemote('ln -s ../../assets/eadmin public/bundles/eadmin');
        $this->runRemote('sudo chown -R web: {{ deploy_dir }}/releases');
    }
    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {

        $this->log('<h1>yarn install + yarn encore production</>');
        $this->runRemote('cd {{ deploy_dir }}/current && yarn install');
        $this->runRemote('cd {{ deploy_dir }}/current && yarn encore production');

        $this->log('<h1>Generate Cache Data District + SVG</>');
        $this->runRemote('{{ console_bin }} app:generate-cachedata svg');
        $this->runRemote('{{ console_bin }} app:generate-cachedata district');
        
        $this->log('<h1>chown -R www-data: /current/var</>');
        $this->runRemote('sudo chown -R www-data: {{ deploy_dir }}/current/var');

    }
};
