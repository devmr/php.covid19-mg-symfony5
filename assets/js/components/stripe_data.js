
document.addEventListener('DOMContentLoaded', () => {
    let bars = Array.prototype.slice.call(document.querySelectorAll('.linepercent'), 0);
    bars.forEach( elem => {

        let coordXBar = parseInt(elem.getAttribute('x1'));
        let strokewidthBar = parseInt(elem.getAttribute('stroke-width'));
        let dateValue = elem.getAttribute('data-date');

        // Mouse enter
        elem.addEventListener('mouseenter', () => {


            elem.closest('svg').querySelector('[data-bubble]').classList.remove('hidden');
            elem.closest('svg').querySelector('[data-text_inside_bubble]').classList.remove('hidden');

            elem.closest('svg').querySelector('[data-bubble]').setAttribute('d',`m ${coordXBar},0.16543192 -0.030308,20.01786608 3.51039,-4.41716 25.6933931,-0.0121 c 2.50742,-1.262046 2.222569,-4.010673 2.222569,-4.010673 l 0.04007,-11.58648641 z`);

            elem.closest('svg').querySelector('[data-text_inside_bubble]').setAttribute('x', coordXBar+strokewidthBar+10);
            elem.closest('svg').querySelector('[data-tooltip_textspan_stripe]').setAttribute('x', coordXBar+strokewidthBar+10);

            elem.closest('svg').querySelector('[data-tooltip_textspan_stripe]').textContent = dateValue;

        })

        // mouse leave
        elem.addEventListener('mouseleave', () => {

            elem.closest('svg').querySelector('[data-bubble]').classList.add('hidden');
            elem.closest('svg').querySelector('[data-text_inside_bubble]').classList.add('hidden');

            elem.closest('svg').querySelector('[data-tooltip_textspan_stripe]').textContent = '';

        })
    })
})
