
document.addEventListener('DOMContentLoaded', () => {
    // Click hamburger menu
    let $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.hamburger'), 0);
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach( $el => {
            $el.addEventListener('click', function() {

                // Add class
                this.classList.toggle("is-active");

                // Get the "main-nav" element
                let $target = document.getElementById('main-nav');

                // Toggle the class on "main-nav"
                $target.classList.toggle('hidden');

            });
        });
    }

    // Switch lang
    let $toggleLangButton = Array.prototype.slice.call(document.querySelectorAll('.dropdown button'), 0);
    if ($toggleLangButton.length > 0) {

        // Add a click event on each of them
        $toggleLangButton.forEach( $el => {
            $el.addEventListener('click', function() {

                // Add class
                this.parentNode.querySelector('.dropdown-menu').classList.toggle("hidden");

            });
        });
    }
});
