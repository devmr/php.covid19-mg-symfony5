import Chart from 'chart.js';

import * as chart from '../chartjs/utils';

let chartsDom = Array.prototype.slice.call(document.querySelectorAll('[data-chartjs]'), 0);
if (chartsDom.length > 0) {

    chartsDom.forEach( elem => {

        let type = elem.getAttribute('data-charts-type');

        switch (type) {
            case 'bar':
                 chart.renderBar(elem);
                break;

            case 'line':
                chart.renderLine(elem);
                break;

            case 'pie':
                chart.renderPie(elem);
                break;
        }
    })
}
