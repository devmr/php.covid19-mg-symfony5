import { CountUp } from 'countup.js';

document.addEventListener('DOMContentLoaded', () => {

    let countUpConfirmed = new CountUp('text-circle-confirmed', document.getElementById('text-circle-confirmed').textContent, {decimal:'.'});
    countUpConfirmed.start();

    let countUpRecovered = new CountUp('text-circle-recovered', document.getElementById('text-circle-recovered').textContent, {decimal:'.'});
    countUpRecovered.start();

});
