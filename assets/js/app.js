/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// Fontawesome
import '@fortawesome/fontawesome-free/scss/fontawesome.scss';
import '@fortawesome/fontawesome-free/scss/regular.scss';
import '@fortawesome/fontawesome-free/scss/solid.scss';
import '@fortawesome/fontawesome-free/scss/brands.scss';

// Tailwind
import 'tailwindcss/dist/base.css';
import 'tailwindcss/dist/components.css';
import 'tailwindcss/dist/utilities.css';

// popper - tippy
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/animations/scale.css'

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';

// Tooltip
import './components/tippy';

// MapSVG
import './components/mapsvg';

// Chartjs
// import './components/chartjs';

// Apexcharts
import './components/apexcharts';

// Stripe
import './components/stripe_data';

// Badger Accordion
import './components/accordion';

// Navigation
import './components/navigation';

// Countup
import './components/countup';
