
import React from 'react';
import ReactDOM from 'react-dom';

import Items from './items';


class App extends React.Component {
    constructor() {
        super();

        this.state = {
            planets: []
        };
    }

    componentDidMount() {
        fetch('/ajax/planet')
            .then(response => response.json())
            .then(planets => {
                this.setState({
                    planets: planets.planets
                });
            });
    }

    render() {
        return (
            <div className="container">
            <div className="row">
            {this.state.planets.map(
                    ({ id, img, name, moons, diameter, distanceFromSun, wikiUrl }) => (
                        <Items
                key={id}
                img={img}
                name={name}
                moons={moons}
                diameter={diameter}
                distanceFromSun={distanceFromSun}
                wikiUrl={wikiUrl}
                >
                </Items>

    )
    )}
    </div>
        </div>
    );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));